#pragma once

#define JData COption::Instance.GetOptionData()

class COption
{
public:
	static COption& Instance() {
		static COption* instance = nullptr;

		if (instance == nullptr) {
			instance = new COption;
		}
		return *instance;
	}

	void Load() {
		std::ifstream input_file("Resource/option.json");
		input_file >> _optionData;
		input_file.close();
	}

	void Save() {
		std::ofstream output_file("Resource/json_data.json");
		output_file << std::setw(4) << _optionData;
		output_file.close();
	}

	json& GetOptionData() {
		return _optionData;
	}

private:
	COption() {}
	~COption() {}

private:
	json _optionData;

};

/*
	//json jData;
	//jData["key1"] = 100;
	//jData["key2"] = 101;
	//jData["key3"] = 102;
	//jData["array1"] = { 4, 3, 2, 1, 5, 3, 10 };
	//jData["obect1"] = { { "key1", 1000 }, { "key2", 2000 } };

	//std::cout << std::setw(4) << jData;

	//std::ofstream output_file("Resource/json_data.json");
	//output_file << std::setw(4) << jData;
	//output_file.close();
*/