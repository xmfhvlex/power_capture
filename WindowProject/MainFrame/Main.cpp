#include "stdafx.h"
#include "WindowFramework.h"

#include "SoundDeviceManager.h"
#include "Option.h"

#define MAX_LOADSTRING 100

WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_WINDOWPROJECT, szWindowClass, MAX_LOADSTRING);
	
	WindowFramework.Initialize(hInstance) ? NULL : exit(-1);

	//Create Instance & Initializing
	SoundDeviceManager;

	WindowFramework.Run();

	return NULL;
}