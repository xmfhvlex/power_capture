#pragma once

namespace KEY {
	constexpr auto FIRST_TIME = "FIRST_TIME";
	constexpr auto DIRECTORY_INDEX = "DIRECTORY_INDEX";
	constexpr auto DIRECTORIES = "DIRECTORIES";
	constexpr auto IMAGE_EXPLORER = "IMAGE_EXPLORER";
	constexpr auto OUT_LINE_DRAW = "OUT_LINE_DRAW";
	constexpr auto DEBUG_MODE = "DEBUG_MODE";
	constexpr auto REGION_SELECT_SIZE = "REGION_SELECT_SIZE";
	constexpr auto SAVE_KEY = "SAVE_KEY";
	constexpr auto DIRECTORY_KEY = "DIRECTORY_KEY";

	constexpr auto COLOR_CAPTURE_BACKGROUND = "COLOR_CAPTURE_BACKGROUND";
	constexpr auto COLOR_CAPTURE_LINE = "COLOR_CAPTURE_LINE";
	constexpr auto COLOR_CAPTURE_OUT_LINE = "COLOR_CAPTURE_OUT_LINE";
	constexpr auto COLOR_OUT_LINE = "COLOR_OUT_LINE";
	constexpr auto COLOR_TEXT = "COLOR_TEXT";
	constexpr auto COLOR_TEXT_BAR = "COLOR_TEXT_BAR";
}

//#define GET_MACRO(_1,_2,_3,NAME,...) NAME
//#define FOO(...) GET_MACRO(__VA_ARGS__, FOO3, FOO2)(__VA_ARGS__)
//

#define JData				COption::Instance().GetOptionData()
#define JDatas(X, Y)		COption::Instance().Data<X>(Y)
#define Option				COption::Instance()

class COption
{
public:
	static COption& Instance() {
		static COption* instance = nullptr;

		if (instance == nullptr) {
			instance = new COption;
			instance->Load();
		}
		return *instance;
	}

	static COption* AA() {
		static COption* instance = nullptr;

		if (instance == nullptr) {
			instance = new COption;
			instance->Load();
		}
		return instance;
	}

	void Load() {
		std::ifstream input_file("Resource/option.json");
		input_file >> _optionData;
		input_file.close();

		for (auto directory : _optionData[KEY::DIRECTORIES]) 
		{
			cout << directory << endl;
		}

		//프로그램이 처음 실행되었을때 Default 폴더 경로를 만든다.
		if (_optionData[KEY::FIRST_TIME] == true)
		{
			char wcPath[512];
			SHGetFolderPathA(NULL, CSIDL_DESKTOP, NULL, SHGFP_TYPE_CURRENT, wcPath);
			strcat_s(wcPath, "\\Captured\\");
			_optionData[KEY::DIRECTORIES].push_back(wcPath);
			_optionData[KEY::DIRECTORIES].push_back(wcPath);
			_optionData[KEY::DIRECTORIES].push_back(wcPath);
			_optionData[KEY::DIRECTORIES].push_back(wcPath);
			_optionData[KEY::FIRST_TIME] = false;
			Save();
		}
	}

	void Save() {
		std::ofstream output_file("Resource/option.json");
		output_file << std::setw(4) << _optionData;
		output_file.close();
	}

	json& GetOptionData() {
		return _optionData;
	}

	template<class T, class KEY>
	T Data(KEY key)
	{
		auto jsonObject = _optionData[key];

		std::vector<float> v{ jsonObject.begin(), jsonObject.end() };
		return *reinterpret_cast<T*>(v.data());
	}

private:
	COption() {}
	~COption() {}

private:
	json _optionData;

};