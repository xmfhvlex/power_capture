#pragma once
#include "Object.h"
#include "Timer.h"

_declspec(selectany) float gfElapsedTime = 0;

#if defined(CLSID_WICImagingFactory)
#undef CLSID_WICImagingFactory
#endif

enum class EMessage {
	ScreenCaptured,
	SoundDeviceChanged,
	SaveDirectoryChanged,
};

#define D2DFramework	CD2DFramework::Instance()
class CD2DFramework{
public:
	static CD2DFramework * Instance();

	void Initialize(HWND hWnd);
	void Release();

	void Update(float deltaTime);
	void ClearScreen();
	void Render(POINT preMousePos, POINT nowMousePos);
	void ScreenShotEffect(POINT preMousePos, POINT newMousePos);
	void DrawOutLine();

	const ComPtr<ID2D1Bitmap>& CreateD2DBitmap(const HBITMAP & hBitmap);
	const ComPtr<ID2D1Bitmap>& GetCapturedBitmap() {	return m_pd2dBitmap; }

	void Message(EMessage msg);
	bool KeyInputProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

private:
	CD2DFramework() {}
	~CD2DFramework() {}

private:
	HWND							m_hWnd;
	ComPtr<ID2D1Factory>			m_pD2DFactory;
	ComPtr<ID2D1HwndRenderTarget>	m_pRenderTarget = nullptr;
	ComPtr<ID2D1RenderTarget>		m_pCapturedRenderTarget = nullptr;
	ComPtr<ID2D1SolidColorBrush>	m_pBrush = nullptr;
	ComPtr<ID2D1StrokeStyle>		m_pStrokeStyle = nullptr;

	ComPtr<IDWriteFactory>			m_pWriteFactory = nullptr;
	ComPtr<IDWriteTextFormat>		m_pTextFormat = nullptr;

	ComPtr<IWICImagingFactory>		m_pWicFactory = nullptr;
	ComPtr<ID2D1Bitmap>				m_pd2dBitmap = nullptr;

	std::list<CObject*>				m_lpUIObject;
};
