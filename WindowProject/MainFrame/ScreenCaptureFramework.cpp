#include "stdafx.h"
#include "ScreenCaptureFramework.h"
#include "WindowFramework.h"
#include "Utility.h"
#include "D2DFramework.h"
#include "Resource.h"
#include "Option.h"

#include <algorithm>

void CScreenCaptureFramework::CreateInstance(HWND hWnd) {
	if (!m_pInstance) {
		m_pInstance = new CScreenCaptureFramework;
		m_pInstance->Initialize(hWnd);
	}
}

CScreenCaptureFramework * CScreenCaptureFramework::GetInstance() {
	return m_pInstance;
}

void CScreenCaptureFramework::Initialize(HWND hWnd) {
	m_hWnd = hWnd;

	hMenu = CreatePopupMenu();
	hSubMenu = CreatePopupMenu();
	int index = 0;
	InsertMenu(hMenu,index++,MF_BYPOSITION | MF_POPUP | MF_STRING, (UINT_PTR)hSubMenu, L"Option");
	InsertMenu(hMenu,index++,MF_BYPOSITION | MF_STRING,TB_OPEN_FOLDER,L"Open_Folder");
	InsertMenu(hMenu,index++,MF_BYPOSITION | MF_STRING,TB_CAPTURE,L"Capture");
	InsertMenu(hMenu,index++,MF_BYPOSITION | MF_STRING,TB_INFO,L"About");
	InsertMenu(hMenu,index++,MF_BYPOSITION | MF_STRING,TB_EXIT,L"Exit");

	index = 0;
	InsertMenu(hSubMenu,index++,MF_BYPOSITION | MF_STRING,TB_SET_DIRECTORY,L"Change Save Directory");
	InsertMenu(hSubMenu, index++, MF_BYPOSITION | MF_STRING, TB_RELOAD_OPTION, L"Reload Option");

//	InsertMenu(hSubMenu,index++,MF_BYPOSITION | MF_STRING,TB_SET_LINE_DRAW,L"On/Off Line Draw");
//	InsertMenu(hSubMenu,index++,MF_BYPOSITION | MF_STRING,TB_SET_LINE_COLOR,L"Change Line Color");
//	InsertMenu(hSubMenu,index++,MF_BYPOSITION | MF_STRING,TB_DEFAULT,L"Set Default");

	//Hot key
	std::string saveKey = JData[KEY::SAVE_KEY];
	std::transform(saveKey.begin(), saveKey.end(), saveKey.begin(), ::toupper);
	std::string directoryKey = JData[KEY::DIRECTORY_KEY];
	std::transform(directoryKey.begin(), directoryKey.end(), directoryKey.begin(), ::toupper);

	RegisterHotKey(hWnd, HK_SCREENSHOT, MOD_CONTROL | MOD_SHIFT | MOD_ALT, saveKey[0]);
	RegisterHotKey(hWnd, HK_OPEN_LAST_SCREENSHOT, MOD_CONTROL | MOD_SHIFT | MOD_ALT, 0x20);
	RegisterHotKey(hWnd, HK_OPEN_DIRECTORY, MOD_CONTROL | MOD_SHIFT | MOD_ALT, directoryKey[0]);

	//Switch Save Directory
	RegisterHotKey(hWnd, HK_LEFT_DIRECTORY, MOD_CONTROL | MOD_SHIFT | MOD_ALT, 0X25);
	RegisterHotKey(hWnd, HK_RIGHT_DIRECTORY, MOD_CONTROL | MOD_SHIFT | MOD_ALT, 0x27);
	
	//Switch Monitor Presentation
	RegisterHotKey(hWnd, HK_MONITOR_SWITCH_EXTEND, MOD_CONTROL | MOD_SHIFT | MOD_ALT, 0x4D);
	RegisterHotKey(hWnd, HK_MONITOR_SWITCH_ONLY_MAIN, MOD_CONTROL | MOD_SHIFT | MOD_ALT, 0xBC);
	RegisterHotKey(hWnd, HK_MONITOR_SWITCH_ONLY_SUB, MOD_CONTROL | MOD_SHIFT | MOD_ALT, 0xBE);
	RegisterHotKey(hWnd, HK_MONITOR_SWITCH_DUPLICATE, MOD_CONTROL | MOD_SHIFT | MOD_ALT, 0xBF);

#ifdef _DEBUG
	RegisterHotKey(hWnd, HK_QUIT, MOD_CONTROL | MOD_SHIFT | MOD_ALT, 'Q');
#endif
}



void CScreenCaptureFramework::Release() {
	D2DFramework->Release();
}

void CScreenCaptureFramework::Update(float deltaTime) {
	D2DFramework->Update(deltaTime);
}

void CScreenCaptureFramework::Render() {
	D2DFramework->Render(m_preMousePos, m_nowMousePos);
}

void CScreenCaptureFramework::FullCapture() 
{
	if (g_bCaptured) {
		return;
	}
	g_bCaptured = true;

	GetCursorPos(&m_preMousePos);
	GetCursorPos(&m_nowMousePos);

	DWORD nFileSize = sizeof(BITMAPINFOHEADER) + (sizeof(RGBTRIPLE) + 1 * (SW_WIDTH * SW_HEIGHT* 4));
	BYTE* pBmpFileData = (BYTE*)GlobalAlloc(0x0040, nFileSize);

	PBITMAPFILEHEADER BFileHeader = (PBITMAPFILEHEADER)pBmpFileData;
	BFileHeader->bfType = 0x4D42; // BM
	BFileHeader->bfSize = sizeof(BITMAPFILEHEADER);
	BFileHeader->bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

	PBITMAPINFOHEADER BInfoHeader = (PBITMAPINFOHEADER)&pBmpFileData[sizeof(BITMAPFILEHEADER)];
	BInfoHeader->biSize = sizeof(BITMAPINFOHEADER);
	BInfoHeader->biPlanes = 1;
	BInfoHeader->biBitCount = 24;
	BInfoHeader->biCompression = BI_RGB;
	BInfoHeader->biWidth = SW_WIDTH;
	BInfoHeader->biHeight = SW_HEIGHT;

	HWND hInputWnd = GetDesktopWindow();
	HDC desctopDC = GetDC(NULL);
	m_hCapturedDC = CreateCompatibleDC(desctopDC);
	m_hCapturedBitmap = CreateCompatibleBitmap(desctopDC, SW_WIDTH, SW_HEIGHT);
	SelectObject(m_hCapturedDC, m_hCapturedBitmap);

	BitBlt(m_hCapturedDC, 
			0, 0, 
			SW_WIDTH, SW_HEIGHT,
			desctopDC, 
			HW_POS_X, HW_POS_Y, 
			SRCCOPY | CAPTUREBLT);

	RGBTRIPLE *pImage = (RGBTRIPLE*)&pBmpFileData[sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER)];
	GetDIBits(m_hCapturedDC, m_hCapturedBitmap, 0, SW_HEIGHT, pImage, (LPBITMAPINFO)BInfoHeader, DIB_RGB_COLORS);

	ReleaseDC(NULL, m_hCapturedDC);
	GlobalFree(pBmpFileData);
	DeleteObject(m_hCapturedBitmap);

	D2DFramework->CreateD2DBitmap(m_hCapturedBitmap);
	SetCursor(LoadCursor(NULL, MAKEINTRESOURCE(IDC_CURSOR1)));
	WindowFramework.SendEvent("SetMessageRoutine", EMessageRoutine::PEEK_MESSAGE);
	WindowFramework.SendEvent("WindowShow", true);
}

void CScreenCaptureFramework::SelectCapture() 
{
	POINT MousePos; 
	GetCursorPos(&MousePos);
	HMONITOR hMonitor = MonitorFromPoint(MousePos, 0);
	MONITORINFO infoMonitor;
	infoMonitor.cbSize = sizeof(MONITORINFO);
	GetMonitorInfo(hMonitor, &infoMonitor);
	int selectedSizeX = infoMonitor.rcMonitor.right - infoMonitor.rcMonitor.left;
	int selectedSizeY = infoMonitor.rcMonitor.bottom - infoMonitor.rcMonitor.top;

	//화면 배율이 100%가 아닌 경우 처리.
	//selectedSizeX *= static_cast<float>(SW_WIDTH) / HW_WIDTH;
	//selectedSizeY *= static_cast<float>(SW_HEIGHT) / HW_HEIGHT;

	DWORD nFileSize = sizeof(BITMAPINFOHEADER) + (sizeof(RGBTRIPLE) + 1 * (selectedSizeX * selectedSizeY * 4));
	BYTE* pBmpFileData = (BYTE*)GlobalAlloc(0x0040, nFileSize);

	PBITMAPFILEHEADER BFileHeader = (PBITMAPFILEHEADER)pBmpFileData;
	BFileHeader->bfType = 0x4D42; // BM
	BFileHeader->bfSize = sizeof(BITMAPFILEHEADER);
	BFileHeader->bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

	PBITMAPINFOHEADER BInfoHeader = (PBITMAPINFOHEADER)&pBmpFileData[sizeof(BITMAPFILEHEADER)];
	BInfoHeader->biSize = sizeof(BITMAPINFOHEADER);
	BInfoHeader->biPlanes = 1;
	BInfoHeader->biBitCount = 24;
	BInfoHeader->biCompression = BI_RGB;
	BInfoHeader->biWidth = selectedSizeX;
	BInfoHeader->biHeight = selectedSizeY;

	m_hSelectedDC = CreateCompatibleDC(m_hCapturedDC);
	m_hSelectedBitmap = CreateCompatibleBitmap(m_hCapturedDC, selectedSizeX, selectedSizeY);
	SelectObject(m_hSelectedDC, m_hSelectedBitmap);

	BitBlt(m_hSelectedDC, 
		0, 
		0, 
		selectedSizeX, 
		selectedSizeY,
		m_hCapturedDC, 
		//Main 모니터 왼쪽(-) 영역에 있는 모니터를 위한 좌표 설정.
		infoMonitor.rcMonitor.left - HW_POS_X, 
		infoMonitor.rcMonitor.top - HW_POS_Y, 
		SRCCOPY
	);

	RGBTRIPLE *pImage = (RGBTRIPLE*)&pBmpFileData[sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER)];
	GetDIBits(m_hSelectedDC, m_hSelectedBitmap, 0, selectedSizeY, pImage, (LPBITMAPINFO)BInfoHeader, DIB_RGB_COLORS);

	ReleaseDC(NULL, m_hSelectedDC);
	GlobalFree(pBmpFileData);
	DeleteObject(m_hSelectedBitmap);
}

void CScreenCaptureFramework::RegionCapture()
{
	float fPreRatioX = (float)SW_WIDTH / HW_WIDTH;
	float fPreRatioY = (float)SW_HEIGHT / HW_HEIGHT;
	float fNowRatioX = (float)SW_WIDTH / HW_WIDTH;
	float fNowRatioY = (float)SW_HEIGHT / HW_HEIGHT;

	//m_preMousePos.x *= fPreRatioX;
	//m_preMousePos.y *= fPreRatioY;
	//m_nowMousePos.x *= fNowRatioX;
	//m_nowMousePos.y *= fNowRatioY;

	if (m_nowMousePos.x < m_preMousePos.x)
		std::swap(m_nowMousePos.x, m_preMousePos.x);
	if (m_nowMousePos.y < m_preMousePos.y) 
		std::swap(m_nowMousePos.y, m_preMousePos.y);
	int nWidth = abs(m_nowMousePos.x - m_preMousePos.x);
	int nHeight = abs(m_nowMousePos.y - m_preMousePos.y);

	/*nWidth /= fPreRatioX;
	nHeight /= fPreRatioY;*/

	DWORD nFileSize = sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)+(sizeof(RGBTRIPLE)+1*(nWidth*nHeight*4));
	BYTE* pBmpFileData = (BYTE*)GlobalAlloc(0x0040,nFileSize);

	PBITMAPFILEHEADER BFileHeader = (PBITMAPFILEHEADER)pBmpFileData;
	BFileHeader->bfType = 0x4D42; // BM
	BFileHeader->bfSize = sizeof(BITMAPFILEHEADER);
	BFileHeader->bfOffBits = sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER);

	PBITMAPINFOHEADER BInfoHeader = (PBITMAPINFOHEADER)&pBmpFileData[sizeof(BITMAPFILEHEADER)];
	BInfoHeader->biSize = sizeof(BITMAPINFOHEADER);
	BInfoHeader->biPlanes = 1;
	BInfoHeader->biBitCount = 24;
	BInfoHeader->biCompression = BI_RGB;
	BInfoHeader->biWidth = nWidth;
	BInfoHeader->biHeight = nHeight;

	m_hSelectedDC = CreateCompatibleDC(m_hCapturedDC);
	m_hSelectedBitmap = CreateCompatibleBitmap(m_hCapturedDC,nWidth,nHeight);
	SelectObject(m_hSelectedDC,m_hSelectedBitmap);

	//SetStretchBltMode(m_hCapturedDC, HALFTONE);
	//StretchBlt(m_hSelectedDC,
	//	0,
	//	0,
	//	nWidth,
	//	nHeight,
	//	m_hCapturedDC, 
	//	m_preMousePos.x - HW_POS_X, 
	//	m_preMousePos.y, 
	//	nWidth,
	//	nHeight,
	//	SRCCOPY|CAPTUREBLT);

	BitBlt(m_hSelectedDC,
		0,
		0,
		nWidth,
		nHeight,
		m_hCapturedDC,
		//Main 모니터 왼쪽(-) 영역에 있는 모니터를 위한 좌표 설정.
		m_preMousePos.x - HW_POS_X,
		m_preMousePos.y - HW_POS_Y,
		SRCCOPY
	);


	RGBTRIPLE *Image = (RGBTRIPLE*)&pBmpFileData[sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)];
	GetDIBits(m_hSelectedDC,m_hSelectedBitmap,0,nHeight,Image,(LPBITMAPINFO)BInfoHeader,DIB_RGB_COLORS);

	ReleaseDC(m_hWnd, m_hCapturedDC);
	ReleaseDC(m_hWnd, m_hSelectedDC);
	GlobalFree(pBmpFileData); 
	DeleteObject(m_hSelectedBitmap);
}

void CScreenCaptureFramework::DrawOutLine() {
	if (JData[KEY::OUT_LINE_DRAW] == false)
		return;

	D2DFramework->DrawOutLine();
}

void CScreenCaptureFramework::LoadToClipBoard(){
	OpenClipboard(NULL);
	EmptyClipboard();
	SetClipboardData(CF_BITMAP, m_hSelectedBitmap);
	CloseClipboard();
}

void CScreenCaptureFramework::SaveCaputre()
{
	std::string aDirectory = JData[KEY::DIRECTORIES][JData[KEY::DIRECTORY_INDEX].get<int>()];
	std::wstring wDirectory(aDirectory.begin(), aDirectory.end());
	
	if (CreateDirectory(wDirectory.c_str(), NULL) || ERROR_ALREADY_EXISTS == GetLastError());

	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	Bitmap bitmap(m_hSelectedBitmap, NULL);
	CLSID clsid;
	GetEncoderClsid(L"image/png", &clsid);
	
	_lastScreenshotFileName = wDirectory + GetTimeW(time(NULL)) + std::wstring(L".png");
	bitmap.Save(_lastScreenshotFileName.c_str(), &clsid);
}

//std::wifstream optionFile;
//bool CScreenCaptureFramework::OperationExecute(const std::wstring& wsOperation)
//{
//	STR_SWITCH_BEGIN(wsOperation.c_str())
//	{
//		CASE(L"First_Execution")
//			optionFile >> m_bFirstExecution;
//			break;
//		CASE(L"Directories")
//			if (m_bFirstExecution) {	//프로그램이 처음 실행되었을 때
//				WCHAR wcPath[512];
//				SHGetFolderPath(NULL, CSIDL_DESKTOP, NULL, SHGFP_TYPE_CURRENT, wcPath);
//				lstrcat(wcPath, L"\\Captured\\");
//				g_vwsSaveDirectory.push_back(wcPath);
//				g_vwsSaveDirectory.push_back(wcPath);
//				g_vwsSaveDirectory.push_back(wcPath);
//				g_vwsSaveDirectory.push_back(wcPath);
//				m_bFirstExecution = false;
//			}
//			else {						//이전에 프로그램을 실행했던 전력이 있을 때
//				std::wstring pivot;
//				while (!optionFile.eof()) {
//					optionFile >> pivot;
//					if (OperationExecute(pivot)) {
//						return true;
//					}
//					g_vwsSaveDirectory.push_back(pivot);
//				}
//			}
//			break;
//		CASE(L"LineDraw")
//			break;
//		CASE(L"End")
//			return true;
//	}
//	STR_SWITCH_END()
//
//	return false;
//}

void CScreenCaptureFramework::MouseInput(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message){
	case WM_RBUTTONDOWN:
		m_bLeftMouseButton = false;
		if(g_bCaptured){
			g_bCaptured = false;
//			SetCursor(LoadCursor(0, MAKEINTRESOURCE(IDC_ARROW)));
			D2DFramework->ClearScreen();
		//	ShowWindow(hWnd, false);
		}
		break;
	case WM_MOUSEMOVE:
		if (!g_bCaptured) {
			break;
		}
		if(m_bLeftMouseButton){
			GetCursorPos(&m_nowMousePos);
		}
		break;
	case WM_LBUTTONDOWN:
		if (!g_bCaptured) {
			break;
		}
		m_bLeftMouseButton = true;
		GetCursorPos(&m_preMousePos);
		GetCursorPos(&m_nowMousePos);
		break;
	case WM_LBUTTONUP:
		if (!g_bCaptured) {
			break;
		}
		GetCursorPos(&m_nowMousePos);
		//Full Screen Capture
		if(abs(m_preMousePos.x - m_nowMousePos.x) * abs(m_preMousePos.y - m_nowMousePos.y) < JData[KEY::REGION_SELECT_SIZE]){
			SelectCapture();
		}
		//Region Caputre
		else{
			RegionCapture();
		}

		D2DFramework->CreateD2DBitmap(m_hSelectedBitmap);

		DrawOutLine();
		LoadToClipBoard();
		SaveCaputre();

		D2DFramework->Message(EMessage::ScreenCaptured);
//		D2DFramework->ClearScreen();
		SetCursor(LoadCursor(0, MAKEINTRESOURCE(IDC_ARROW)));
		g_bCaptured = false;
		m_bLeftMouseButton = false;
		break;
	}
}

void CScreenCaptureFramework::KeyInput(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) {
	case WM_KEYDOWN:
		switch (wParam) {
		case VK_ESCAPE:
			if(g_bCaptured){
				g_bCaptured = false;
	//			SetCursor(LoadCursor(0, MAKEINTRESOURCE(IDC_ARROW)));
				D2DFramework->ClearScreen();
			//	ShowWindow(hWnd, false);
			}
			break;
		}
		break;
	case WM_KEYUP:
		switch (wParam) {
		}
		break;
	}
}

bool CScreenCaptureFramework::HotKey(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (wParam) {
	case HK_SCREENSHOT: {
		FullCapture();
		return true;
	}
	case HK_OPEN_DIRECTORY: {
		std::string sDirectory = JData[KEY::DIRECTORIES][JData[KEY::DIRECTORY_INDEX].get<int>()];
		std::wstring wstrDirectory(sDirectory.begin(), sDirectory.end());
		ShellExecute(NULL, TEXT("open"), NULL, NULL, wstrDirectory.c_str(), SW_SHOW);
		return true;
	}
	case HK_LEFT_DIRECTORY:
		JData[KEY::DIRECTORY_INDEX] = JData[KEY::DIRECTORY_INDEX] - 1;
		if (JData[KEY::DIRECTORY_INDEX] == -1) {
			JData[KEY::DIRECTORY_INDEX] = JData[KEY::DIRECTORIES].size() - 1;
		}

		cout << JData[KEY::DIRECTORY_INDEX] << endl;

		Option.Save();
		D2DFramework->Message(EMessage::SaveDirectoryChanged);
		return true;

	case HK_RIGHT_DIRECTORY:
		JData[KEY::DIRECTORY_INDEX] = JData[KEY::DIRECTORY_INDEX] + 1;
		if (JData[KEY::DIRECTORY_INDEX] == JData[KEY::DIRECTORIES].size()) {
			JData[KEY::DIRECTORY_INDEX] = 0;
		}
		Option.Save();
		D2DFramework->Message(EMessage::SaveDirectoryChanged);
		return true;

	case HK_OPEN_LAST_SCREENSHOT: {
		std::ifstream file(_lastScreenshotFileName);
		if (file.good() == false)
			break;

		std::string imageExplorer = JData[KEY::IMAGE_EXPLORER];
		std::wstring wstrimageExplorer(imageExplorer.begin(), imageExplorer.end());
		ShellExecute(NULL, TEXT("open"), wstrimageExplorer.c_str(), _lastScreenshotFileName.c_str(), NULL, SW_SHOW);
		//ShellExecute(NULL, TEXT("open"), TEXT("D:\\01_Program\\HoneyView3\\HoneyView3.exe"), _lastScreenshotFileName.c_str(), NULL, SW_SHOW);
		break;
	}
	case HK_MONITOR_SWITCH_EXTEND:
		ShellExecute(NULL, TEXT("open"), TEXT("C:\\Windows\\System32\\DisplaySwitch.exe"), TEXT("/extend"), NULL, NULL);
		break;
	case HK_MONITOR_SWITCH_ONLY_MAIN:
		ShellExecute(NULL, TEXT("open"), TEXT("C:\\Windows\\System32\\DisplaySwitch.exe"), TEXT("/internal"), NULL, NULL);
		break;
	case HK_MONITOR_SWITCH_ONLY_SUB:
		ShellExecute(NULL, TEXT("open"), TEXT("C:\\Windows\\System32\\DisplaySwitch.exe"), TEXT("/external"), NULL, NULL);
		break;
	case HK_MONITOR_SWITCH_DUPLICATE:
		ShellExecute(NULL, TEXT("open"), TEXT("C:\\Windows\\System32\\DisplaySwitch.exe"), TEXT("/clone"), NULL, NULL);
		break;
	}
	return false;
}

void CScreenCaptureFramework::TaskBar(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	POINT mouse;

	switch(lParam){
	case WM_LBUTTONDBLCLK:
		break;
	case WM_LBUTTONDOWN:
		TrackPopupMenu(NULL,NULL,NULL,NULL,0,hWnd,NULL);
		break;
	case WM_RBUTTONUP:
		GetCursorPos(&mouse);
		TrackPopupMenu(hMenu,TPM_BOTTOMALIGN | TPM_LEFTALIGN | TPM_LEFTBUTTON,mouse.x,mouse.y,0,hWnd,NULL);
		break;
	}
}

void CScreenCaptureFramework::MenuSelect(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (LOWORD(wParam)) {
	case TB_EXIT:
		g_bLoop = false;
		break;
	case TB_INFO: {
		std::wstring str;
		str += L"        For your Magnificent Data Collecting\n";
		str += L"\n";
		str += L"                 by - Patric Cho (조경배)\n";
		str += L"\n";
		str += L"             Capture : Ctrl + Alt + Shift + S\n";
		str += L"\n";
		str += L"        Directory Open : Ctrl + Alt + Shift + D\n";
		str += L"\n";
		str += L"    Directory Change : Ctrl + Alt + Shift + (←, →)\n";
		str += L"\n";
		str += L" Sound Device Change : Ctrl + Alt + Shift + (↑, ↓)\n";
		str += L"\n";
		str += L"            Bug Report : xmfhvlex@naver.com\n";
		str += L"\n";
		str += L"         Blog : http://blog.naver.com/xmfhvlex \n";
		MessageBox(hWnd, str.c_str(), L"Power Capture ver_0.05", MB_OK | MB_ICONQUESTION);
		break;
	}
	case TB_CAPTURE:
		FullCapture();
		break;
	case TB_SET_DIRECTORY: {
		char cpath[512] = "";
		LPITEMIDLIST pDirList;
		BROWSEINFOA browseInfo;
		browseInfo.hwndOwner = hWnd;
		browseInfo.pidlRoot = NULL;
		browseInfo.lpszTitle = "Select The Folder for Your Captures";
		browseInfo.pszDisplayName = cpath;
		browseInfo.ulFlags = BIF_RETURNONLYFSDIRS;
		browseInfo.lpfn = NULL;
		browseInfo.lParam = 0;

		pDirList = SHBrowseForFolderA(&browseInfo);
		if (pDirList == NULL) return;
		SHGetPathFromIDListA(pDirList, cpath);

		JData[KEY::DIRECTORIES][JData[KEY::DIRECTORY_INDEX].get<int>()] = std::string(cpath) + "\\";
		Option.Save();
		break;
	}
	case TB_RELOAD_OPTION:
		Option.Load();
		break;
	case TB_SET_LINE_DRAW:
		//	m_bLineDraw = 1 - m_bLineDraw;
		Option.Save();
		break;
	case TB_SET_LINE_COLOR: {
		//CHOOSECOLOR color;
		//COLORREF ccref[16];
		//COLORREF selcolor=0x000000;
		//memset(&color,0,sizeof(color));
		//color.lStructSize=sizeof(CHOOSECOLOR);
		//color.hwndOwner=hWnd;
		//color.lpCustColors=ccref;
		//color.rgbResult=selcolor;
		//color.Flags=CC_RGBINIT;
		//ChooseColor(&color);
		//m_dColor = color.rgbResult;
		Option.Save();
		break;
	}
	case TB_OPEN_FOLDER: {
		std::string sDirectory = JData[KEY::DIRECTORIES][JData[KEY::DIRECTORY_INDEX].get<int>()];
		std::wstring wstrDirectory(sDirectory.begin(), sDirectory.end());
		ShellExecute(NULL, TEXT("explore"), NULL, NULL, wstrDirectory.c_str(), SW_SHOW);
		break;
	}
	case TB_DEFAULT:
	//	g_vwsSaveDirectory.clear();
	//	g_vwsSaveDirectory.push_back(L".\\");
		Option.Save();
		break;
	}
}
