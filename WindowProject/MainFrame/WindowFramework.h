#pragma once

#include "resource.h"

__declspec(selectany) HWND g_hwnd;
__declspec(selectany) HINSTANCE	g_hInst;
__declspec(selectany) bool g_bCaptured = false;

//Hot Key
enum{
	//Screen Capture Framework
	HK_SCREENSHOT,
	HK_OPEN_LAST_SCREENSHOT,
	HK_OPEN_DIRECTORY,
	HK_LEFT_DIRECTORY,
	HK_RIGHT_DIRECTORY,

	//Sound Device Manager
	HK_LEFT_SOUND_DEVICE,
	HK_RIGHT_SOUND_DEVICE,

	//Monitor Projection Switch
	HK_MONITOR_SWITCH_EXTEND,
	HK_MONITOR_SWITCH_ONLY_MAIN,
	HK_MONITOR_SWITCH_ONLY_SUB,
	HK_MONITOR_SWITCH_DUPLICATE,

	HK_QUIT,
};

//Task Bar Menu
enum{
	TB_EXIT,
	TB_INFO,
	TB_CAPTURE,
	TB_SET_DIRECTORY,
	TB_SET_LINE_DRAW,
	TB_SET_LINE_COLOR,
	TB_OPEN_FOLDER,
	TB_RELOAD_OPTION,
	TB_DEFAULT,
};

enum EMessageRoutine : byte {
	GET_MESSAGE,
	PEEK_MESSAGE,
};

struct ENUM_DISP_ARG
{
	TCHAR msg[500];
	int monId;
};

#define WindowFramework CWindowFramework::Instance()
class CWindowFramework
{
public:
	static CWindowFramework& Instance()
	{
		static CWindowFramework* intsance;
		if (intsance == nullptr) {
			intsance = new CWindowFramework;
		}
		return *intsance;
	}

	BOOL Initialize(HINSTANCE hInstance);
	ATOM RegisterMyClass(HINSTANCE hInstance);
	void RegisterCallback();
	void Release();

	void Run();

	void AddEvent(std::string event, std::function<void(json)> func);
	void SendEvent(std::string event, json data = NULL);

	static void GetMsgRoutine(const HACCEL& hAccelTable);
	static void PeekMsgRoutine(const HACCEL& hAccelTable);

private:
	CWindowFramework() {}

private:
	std::map<std::string, std::vector<std::function<void(json)>>> _callbacks;
	static std::function<void(const HACCEL& hAccelTable)> MsgRoutine;
	static float _time;
};

__declspec(selectany) std::function<void(const HACCEL& hAccelTable)> CWindowFramework::MsgRoutine;
__declspec(selectany) float CWindowFramework::_time;

void UpdateCoords();
BOOL CALLBACK EnumDispProc(HMONITOR hMon, HDC dcMon, RECT* pRcMon, LPARAM lParam);
LRESULT WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
bool HotKey(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);