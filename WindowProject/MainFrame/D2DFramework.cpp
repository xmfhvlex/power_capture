#include "stdafx.h"
#include "D2DFramework.h"
#include "WindowFramework.h"
#include "SoundDeviceManager.h"
#include "Option.h"

CD2DFramework * CD2DFramework::Instance()
{
	static CD2DFramework * pInstance;

	if (pInstance == nullptr)
	{
		pInstance = new CD2DFramework;
		pInstance->Initialize(g_hwnd);
	}
	return pInstance;
}

void CD2DFramework::Initialize(HWND hWnd)
{
	m_hWnd = hWnd;

	//CObject* pObject = new CObject;
	//IComponent* pUpdateComp = new IUpdateComponent(pObject);
	//IComponent* pRenderComp = new IRenderComponent(pObject);
	//pObject->OperateComponent(ECompType::Update);
	//pObject->OperateComponent(ECompType::ClearScreen);

	Microsoft::WRL::Details::ComPtrRef<ComPtr<IWICImagingFactory>> Hello(&m_pWicFactory);

	//Initialzing IWICImagingFactory
	CoInitialize(nullptr);
	CoCreateInstance(CLSID_WICImagingFactory, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(m_pWicFactory.ReleaseAndGetAddressOf()));

	//Direct2D ClearScreen Target
	D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, m_pD2DFactory.ReleaseAndGetAddressOf());
	//D2D1CreateFactory(D2D1_FACTORY_TYPE_MULTI_THREADED, &m_pD2DFactory);

	D2D1_RENDER_TARGET_PROPERTIES props =
		D2D1::RenderTargetProperties(
			D2D1_RENDER_TARGET_TYPE_HARDWARE,
			D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED),
			96,
			96
		);

	RECT rect;
	GetClientRect(hWnd, &rect);
	D2D1_HWND_RENDER_TARGET_PROPERTIES prop2 = D2D1::HwndRenderTargetProperties(
		hWnd,
		D2D1::SizeU(rect.right, rect.bottom),
		D2D1_PRESENT_OPTIONS_IMMEDIATELY
	);

	//Direct2D HWND Buffer
	m_pD2DFactory->CreateHwndRenderTarget(props, prop2, m_pRenderTarget.ReleaseAndGetAddressOf());

	//Brush
	m_pRenderTarget->CreateSolidColorBrush(D2D1::ColorF(0, 0, 0, 1), m_pBrush.ReleaseAndGetAddressOf());

	//Stroke Style
	float dashes[] = {5.0f, 10.0f};
	m_pD2DFactory->CreateStrokeStyle
	(
		D2D1::StrokeStyleProperties(
			D2D1_CAP_STYLE_FLAT,
			D2D1_CAP_STYLE_FLAT,
			D2D1_CAP_STYLE_ROUND,
			D2D1_LINE_JOIN_MITER,
			100.0f,
			D2D1_DASH_STYLE_CUSTOM,
			0.0f
		),
		dashes, _ARRAYSIZE(dashes),
		m_pStrokeStyle.ReleaseAndGetAddressOf()
	);

	//Text ClearScreen
	DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), reinterpret_cast<IUnknown * *>(m_pWriteFactory.ReleaseAndGetAddressOf()));
	m_pWriteFactory->CreateTextFormat(L"HYGothic",
		nullptr,
		DWRITE_FONT_WEIGHT_NORMAL,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		16.0f, 
		L"", 
		m_pTextFormat.ReleaseAndGetAddressOf());
}

void CD2DFramework::Release() {
	m_pD2DFactory.Reset();
	m_pRenderTarget.Reset();
	m_pBrush.Reset();
	m_pStrokeStyle.Reset();
	m_pWriteFactory.Reset();
	m_pTextFormat.Reset();
	m_pWicFactory.Reset();
	m_pd2dBitmap.Reset();
}


void CD2DFramework::Message(EMessage msg) {
	switch (msg) {
		case EMessage::ScreenCaptured: {
			CObject* pNewObject = new CObject;
			pNewObject->SetSize(VECTOR2(300, 90));
			pNewObject->SetPosition(-pNewObject->GetSize().x, 0);
			ISlidePopUpdateComponent* pUpdateComp = new ISlidePopUpdateComponent(pNewObject);
			ICapturedRenderComponent* pRenderComp = new ICapturedRenderComponent(pNewObject);
			pRenderComp->SetBitmap(m_pd2dBitmap.Get());
			pRenderComp->SetStringData(L"Screen Captured!");
			m_lpUIObject.push_back(pNewObject);
			WindowFramework.SendEvent("SetMessageRoutine", EMessageRoutine::PEEK_MESSAGE);
			WindowFramework.SendEvent("WindowShow", true);
			break;
		}
		case EMessage::SoundDeviceChanged: {
			CObject* pNewObject = new CObject;
			pNewObject->SetSize(VECTOR2(500, 60));
			pNewObject->SetPosition(-pNewObject->GetSize().x, 0);
			ISlidePopUpdateComponent* pUpdateComp = new ISlidePopUpdateComponent(pNewObject);
			ITextRenderComponent* pRenderComp = new ITextRenderComponent(pNewObject);
			std::wstring wDeviceName = SoundDeviceManager->GetNowDeviceName();
			pRenderComp->SetStringData(L"Sound Device Changed!\n" + wDeviceName);
			m_lpUIObject.push_back(pNewObject);
			WindowFramework.SendEvent("SetMessageRoutine", EMessageRoutine::PEEK_MESSAGE);
			WindowFramework.SendEvent("WindowShow", true);
			break;
		}
		case EMessage::SaveDirectoryChanged: {
			CObject* pNewObject = new CObject;
			pNewObject->SetSize(VECTOR2(400, 60));
			pNewObject->SetPosition(-pNewObject->GetSize().x, 0);
			ISlidePopUpdateComponent* pUpdateComp = new ISlidePopUpdateComponent(pNewObject);
			ITextRenderComponent* pRenderComp = new ITextRenderComponent(pNewObject);
			std::string sDirectory = JData[KEY::DIRECTORIES][JData[KEY::DIRECTORY_INDEX].get<int>()];
			std::wstring wstrData = L"Save Directory Changed!\n" + std::wstring(sDirectory.begin(), sDirectory.end());
			pRenderComp->SetStringData(wstrData);
			m_lpUIObject.push_back(pNewObject);
			WindowFramework.SendEvent("SetMessageRoutine", EMessageRoutine::PEEK_MESSAGE);
			WindowFramework.SendEvent("WindowShow", true);
			break;
		}
	}
}

bool CD2DFramework::KeyInputProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
	case WM_KEYUP:
		switch (wParam) {

		}
		break;
	}
	return true;
}

void CD2DFramework::ClearScreen() {
	m_pRenderTarget->BeginDraw();
		m_pRenderTarget->Clear(D2D1::ColorF(1,0,1,1));
	m_pRenderTarget->EndDraw();
}

void CD2DFramework::Update(float deltaTime) {
	m_lpUIObject.remove_if([=](CObject* pUiObject) {
		pUiObject->Update(deltaTime);
		if (pUiObject->IsDeleted()) {
			pUiObject->Release();
			return true;
		}
		else {
			return false;
		}
	});
	if(m_lpUIObject.size()==0 && g_bCaptured==false){
		WindowFramework.SendEvent("SetMessageRoutine", EMessageRoutine::GET_MESSAGE);
		WindowFramework.SendEvent("WindowShow", false);
	}
}

void CD2DFramework::Render(POINT preMousePos, POINT nowMousePos) {
	preMousePos.x -= SW_POS_X;
	preMousePos.y -= SW_POS_Y;

	nowMousePos.x -= SW_POS_X;
	nowMousePos.y -= SW_POS_Y;

	m_pRenderTarget->BeginDraw();
		if(g_bCaptured){
			m_pRenderTarget->DrawBitmap(m_pd2dBitmap.Get(), D2D1::RectF(0, 0, SW_WIDTH, SW_HEIGHT));
			ScreenShotEffect(preMousePos, nowMousePos);
		}
		else{
			m_pRenderTarget->Clear(D2D1::ColorF(1,0,1,1));
			
			//UI 처리
			for (CObject* pUiObject : m_lpUIObject) {
				pUiObject->Render(m_pRenderTarget.Get(), m_pBrush.Get(), m_pTextFormat.Get());
			}
		}
	m_pRenderTarget->EndDraw();
}

void CD2DFramework::ScreenShotEffect(POINT preMousePos, POINT nowMousePos)
{
	POINT MousePos;
	/*
		주 모니터의 위치가 0, 0에서 시작하지 않는 경우
		Monitor의 좌표계와 Window 좌표계가 다를 수 있다.
	*/
	GetCursorPos(&MousePos);
	MousePos.x -= SW_POS_X;
	MousePos.y -= SW_POS_Y;

	if (nowMousePos.x < preMousePos.x) {
		std::swap(nowMousePos.x, preMousePos.x);
	}
	if (nowMousePos.y < preMousePos.y) {
		std::swap(nowMousePos.y, preMousePos.y);
	}

	m_pBrush->SetColor(JDatas(D3DCOLORVALUE, KEY::COLOR_CAPTURE_BACKGROUND));
	m_pRenderTarget->FillRectangle(D2D1::RectF(0, 0, SW_WIDTH, preMousePos.y), m_pBrush.Get());
	m_pRenderTarget->FillRectangle(D2D1::RectF(0, preMousePos.y, preMousePos.x, nowMousePos.y), m_pBrush.Get());
	m_pRenderTarget->FillRectangle(D2D1::RectF(nowMousePos.x, preMousePos.y, SW_WIDTH, nowMousePos.y), m_pBrush.Get());
	m_pRenderTarget->FillRectangle(D2D1::RectF(0, nowMousePos.y, SW_WIDTH, SW_HEIGHT), m_pBrush.Get());

	m_pBrush->SetColor(JDatas(D3DCOLORVALUE, KEY::COLOR_CAPTURE_LINE));
	m_pRenderTarget->DrawLine(D2D1::Point2F(0, MousePos.y), D2D1::Point2F(HW_WIDTH, MousePos.y), m_pBrush.Get(), 0.7, 0);
	m_pRenderTarget->DrawLine(D2D1::Point2F(MousePos.x, 0), D2D1::Point2F(MousePos.x, HW_HEIGHT), m_pBrush.Get(), 0.7, 0);

	m_pBrush->SetColor(JDatas(D3DCOLORVALUE, KEY::COLOR_TEXT_BAR));
	m_pRenderTarget->FillRectangle(D2D1::RectF(0, 0, HW_WIDTH, 30), m_pBrush.Get());

	WCHAR wChar[20];
	swprintf_s(wChar, L"[%d X %d]", MousePos.x, MousePos.y);
	m_pBrush->SetColor(JDatas(D3DCOLORVALUE, KEY::COLOR_TEXT));
	m_pRenderTarget->DrawTextW(wChar, lstrlen(wChar), m_pTextFormat.Get(), D2D1::RectF(MousePos.x+2, MousePos.y-19, MousePos.x+200, MousePos.y), m_pBrush.Get());


	std::string sDirectory = JData[KEY::DIRECTORIES][JData[KEY::DIRECTORY_INDEX].get<int>()];
	std::wstring wsDirectory = L"Save Directory" + std::wstring(sDirectory.begin(), sDirectory.end());
	m_pRenderTarget->DrawTextW(wsDirectory.c_str(), wsDirectory.length(), m_pTextFormat.Get(), D2D1::RectF(0, 0, HW_WIDTH, 10), m_pBrush.Get());

	if (preMousePos.x != nowMousePos.x && preMousePos.y != nowMousePos.y) {
		m_pBrush->SetColor(JDatas(D3DCOLORVALUE, KEY::COLOR_CAPTURE_OUT_LINE));
		m_pRenderTarget->DrawRectangle(D2D1::RectF(preMousePos.x, preMousePos.y, nowMousePos.x, nowMousePos.y), m_pBrush.Get(), 1, m_pStrokeStyle.Get());
	}
}

void CD2DFramework::DrawOutLine()
{
	//m_pCapturedRenderTarget->BeginDraw();

	//m_pBrush->SetColor(JDatas(D3DCOLORVALUE, KEY::COLOR_OUT_LINE));
	//m_pCapturedRenderTarget->FillRectangle(D2D1::RectF(0, 0, 100, 100), m_pBrush);

	//m_pCapturedRenderTarget->EndDraw();
}

const ComPtr<ID2D1Bitmap>& CD2DFramework::CreateD2DBitmap(const HBITMAP & hBitmap)
{
	//if (m_pd2dBitmap) {
	//	m_pd2dBitmap->Release();
	//	m_pd2dBitmap = nullptr;
	//}

	ComPtr<IWICBitmap> pDialogBitmap;
	m_pWicFactory->CreateBitmapFromHBITMAP(hBitmap, nullptr, WICBitmapIgnoreAlpha, pDialogBitmap.GetAddressOf());

	ComPtr<IWICFormatConverter> pConvertedSourceBitmap;
	m_pWicFactory->CreateFormatConverter(&pConvertedSourceBitmap);

	if (pConvertedSourceBitmap != nullptr)
	{
		pConvertedSourceBitmap->Initialize(pDialogBitmap.Get(), GUID_WICPixelFormat32bppPBGRA, WICBitmapDitherTypeNone, NULL, 0.f, WICBitmapPaletteTypeMedianCut);
		m_pRenderTarget->CreateBitmapFromWicBitmap(pConvertedSourceBitmap.Get(), 0, m_pd2dBitmap.GetAddressOf());
	}

/*
	D2D1_RENDER_TARGET_PROPERTIES props =
		D2D1::RenderTargetProperties(
			D2D1_RENDER_TARGET_TYPE_HARDWARE,
			D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED),
			96,
			96
		);
*/

	//m_pD2DFactory->CreateWicBitmapRenderTarget(pDialogBitmap, D2D1::RenderTargetProperties(D2D1_RENDER_TARGET_TYPE_DEFAULT,
	//	D2D1::PixelFormat(DXGI_FORMAT_B8G8R8A8_UNORM,
	//		D2D1_ALPHA_MODE_PREMULTIPLIED),
	//	0.f, 0.f,
	//	D2D1_RENDER_TARGET_USAGE_GDI_COMPATIBLE), &m_pCapturedRenderTarget);


	pDialogBitmap.Reset();
	pConvertedSourceBitmap.Reset();

	return m_pd2dBitmap;
}