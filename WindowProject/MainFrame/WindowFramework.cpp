﻿// WindowProject.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "WindowFramework.h"
#include "ScreenCaptureFramework.h"
#include "SoundDeviceManager.h"
#include "Timer.h"
#include "Utility.h"
#include "Resource.h"
#include "D2DFramework.h"
#include "Option.h"

BOOL CWindowFramework::Initialize(HINSTANCE hInstance)
{
	//Find Old Instance And Close
	auto hwndFound = ::FindWindow(L"MainWindow", L"Power_Capture");
	if (hwndFound != nullptr)
	{
		::SendMessage(hwndFound, WM_CLOSE, NULL, NULL);
	}

	g_hInst = hInstance;
	RegisterMyClass(hInstance);
	UpdateCoords();

	DWORD dwStyle;
	DWORD dwExStyle;
	dwExStyle = WS_EX_LAYERED | WS_EX_TOPMOST;
	dwStyle = WS_POPUP;
#ifdef _DEBUG
	dwExStyle = WS_EX_LAYERED;
	dwStyle = WS_SYSMENU|WS_MINIMIZEBOX|WS_MAXIMIZEBOX;
	#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console")
#endif


//	if (JData[KEY::DEBUG_MODE] == true) 
//	{
//		dwStyle = WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX;
//		dwExStyle = WS_EX_LAYERED;
//#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console")
//	}
//	else {
//		dwExStyle = WS_EX_LAYERED|WS_EX_TOPMOST;
//		dwStyle = WS_POPUP;
//	}


	RECT rect = { SW_POS_X, SW_POS_Y, SW_WIDTH, SW_HEIGHT };

	g_hwnd = CreateWindowEx(
		dwExStyle,
		L"MainWindow",
		L"Power_Capture",
		dwStyle,
		rect.left, rect.top,
		rect.right, rect.bottom,
		NULL, NULL, hInstance, NULL
	);
	SetLayeredWindowAttributes(g_hwnd, RGB(255, 0, 255), 255, ULW_COLORKEY | LWA_ALPHA);

	if (!g_hwnd) 
		return -1;

	ScreenCaptureFramework->CreateInstance(g_hwnd);

	//System Tray
	NOTIFYICONDATA nid;
	nid.cbSize = sizeof(NOTIFYICONDATA);
	nid.hWnd = g_hwnd;
	nid.uID = 100;
	nid.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SMALL));
	nid.uCallbackMessage = WM_TASKBAR;
	wsprintf(nid.szTip, L"Power Capture\nby Gyung Bae Cho");
	nid.hBalloonIcon;
	nid.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP;

	//Remove Previous System Tray
	Shell_NotifyIcon(NIM_DELETE, &nid);

	Sleep(1000);
	//Add New System Tray
	Shell_NotifyIcon(NIM_ADD, &nid);

	ShowWindow(g_hwnd, true);
	ShowWindow(g_hwnd, false);

	RegisterCallback();
	return TRUE;
}

ATOM CWindowFramework::RegisterMyClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcx;
	ZeroMemory(&wcx, sizeof(WNDCLASSEX));
	wcx.cbSize = sizeof(WNDCLASSEX);
	wcx.hbrBackground = (HBRUSH)CreateSolidBrush(RGB(0, 0, 0, 1));
	wcx.hInstance = hInstance;
	wcx.lpfnWndProc = WndProc;
	wcx.lpszClassName = L"MainWindow";
	//	wcx.hCursor			= LoadCursor(nullptr, IDC_ARROW);
	wcx.hCursor = LoadCursor(hInstance, MAKEINTRESOURCE(IDC_CURSOR1));
	wcx.style = CS_HREDRAW | CS_VREDRAW;
	return RegisterClassEx(&wcx);
}

void CWindowFramework::Release() {
	ScreenCaptureFramework->Release();

	//Release Resources
	//System Tray 제거
	NOTIFYICONDATA nid;						
	nid.cbSize = sizeof(NOTIFYICONDATA);
	nid.hWnd = g_hwnd;
	nid.uID = 100;
	Shell_NotifyIcon(NIM_DELETE, &nid);
//	Shell_NotifyIcon(NIM_MODIFY, &nid);
}

void CWindowFramework::Run()
{
	HACCEL hAccelTable = LoadAccelerators(g_hInst, MAKEINTRESOURCE(IDC_WINDOWPROJECT));

	MsgRoutine = PeekMsgRoutine;
	MsgRoutine = GetMsgRoutine;

	// Main message loop:
	while (g_bLoop)
	{
		MsgRoutine(hAccelTable);
	}

	Release();
}

void CWindowFramework::AddEvent(std::string event, std::function<void(json)> func)
{
	_callbacks[event].push_back(func);
}

void CWindowFramework::SendEvent(std::string event, json data)
{
	try {
		auto vec = _callbacks.at(event);

		for each (auto var in vec)
		{
			var(data);
		}
	}
	catch (std::out_of_range) {
		MessageBoxA(g_hwnd, ("Invalid Event : " + event).c_str(), "Invalid Callback Event", NULL);
	}
}

void CWindowFramework::RegisterCallback() 
{
	//윈도우 창 On Off 제어
	AddEvent(
		"WindowShow", 
		[&](json j) {
			InvalidateRect(g_hwnd, NULL, TRUE);
			ShowWindow(g_hwnd, j);
		}
	);

	//메세지 루틴 제어
	AddEvent(
		"SetMessageRoutine",
		[&](json j) {
			switch (static_cast<EMessageRoutine>(j))
			{
			case EMessageRoutine::GET_MESSAGE:
				MsgRoutine = GetMsgRoutine;
				break;
			case EMessageRoutine::PEEK_MESSAGE:
				MsgRoutine = PeekMsgRoutine;
				break;
			}
		}
	);

/*
	AddEvent("aaa2", std::bind(&CWindowFramework::AAA2, this, std::placeholders::_1));
	SendEvent("aaa2", 100);

	AddEvent("aaa3", std::bind(&CWindowFramework::AAA3, this, std::placeholders::_1));
	SendEvent("aaa3", json{ 1, 3, 4, 5, 6, 7, 9 });
*/
}

void CWindowFramework::GetMsgRoutine(const HACCEL & hAccelTable) {
	static MSG msg;
	GetMessage(&msg, nullptr, 0, 0);
	DispatchMessage(&msg);
}


void CWindowFramework::PeekMsgRoutine(const HACCEL & hAccelTable) {
	static MSG msg;
	if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) 
	{
		DispatchMessage(&msg);
	}
	else 
	{
		auto deltaTime = TimerEngine->Update();
	//	std::chrono::system_clock::time_point startTime = std::chrono::system_clock::now();
	//	while ((std::chrono::system_clock::now() - startTime).count() < 1.0f / FRAME_PER_MICRO_SECOND);
		_time += deltaTime;
		if (_time < 0.016)
			return;
		_time = 0;

		ScreenCaptureFramework->Update(deltaTime);
		ScreenCaptureFramework->Render();
	}
}

BOOL CALLBACK MonitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData)
{
	HW_POS_X = min(HW_POS_X, lprcMonitor->left);
	HW_POS_Y = min(HW_POS_Y, lprcMonitor->top);
	HW_WIDTH = max(HW_WIDTH, lprcMonitor->right - HW_POS_X);
	HW_HEIGHT = max(HW_HEIGHT, lprcMonitor->bottom - HW_POS_Y);
	return TRUE;
}

void UpdateCoords() {
	HW_POS_X = MAXINT;
	HW_POS_Y = MAXINT;
	HW_WIDTH = MININT;
	HW_HEIGHT = MININT;

	//Software Coord
	SW_POS_X = GetSystemMetrics(SM_XVIRTUALSCREEN);
	SW_POS_Y = GetSystemMetrics(SM_YVIRTUALSCREEN);
	SW_WIDTH = GetSystemMetrics(SM_CXVIRTUALSCREEN);
	SW_HEIGHT = GetSystemMetrics(SM_CYVIRTUALSCREEN);

	EnumDisplayMonitors(NULL, NULL, MonitorEnumProc, NULL);
}

bool HotKey(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (wParam) {
	case HK_QUIT:
		g_bLoop = false;
		return true;
	}
	return false;
}

LRESULT WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
		//	SetTimer(hWnd, 1, FRAME_PER_MICRO_SECOND, NULL);
		break;
	case WM_COMMAND:
	{
		ScreenCaptureFramework->MenuSelect(hWnd, message, wParam, lParam);
		int wmId = LOWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(g_hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	}
	case WM_DISPLAYCHANGE: 
	{
		cout << "Display Change" << endl;
		Sleep(3000);

		UpdateCoords();
		::SetWindowPos(g_hwnd, NULL, HW_POS_X, HW_POS_Y, HW_WIDTH, HW_HEIGHT, NULL);
		cout << HW_POS_X << " - " << HW_POS_Y << " - " << HW_WIDTH << " - " << HW_HEIGHT << endl;
		break;
	}
	case WM_TIMER:
		break;
	case WM_PAINT:
		ValidateRect(hWnd, NULL);
		break;
	case WM_HOTKEY:
		if (ScreenCaptureFramework->HotKey(hWnd, message, wParam, lParam));		//Screen Capture Framework Hotkey
		else if (SoundDeviceManager->HotKey(hWnd, message, wParam, lParam));	//Sound Device Manager Hotkey
		else HotKey(hWnd, message, wParam, lParam);								//Api Main Framework Hotkey	
		break;
	case WM_TASKBAR:
		ScreenCaptureFramework->TaskBar(hWnd, message, wParam, lParam);
		break;
	case WM_DEVICECHANGE:
		SoundDeviceManager->PrepareDeviceList();
		break;
	case WM_MOUSEMOVE:case WM_LBUTTONDOWN:case WM_LBUTTONUP:case WM_RBUTTONDOWN:
	case WM_RBUTTONUP:case WM_RBUTTONDBLCLK:case WM_MBUTTONDOWN:case WM_MBUTTONUP:
	case WM_MOUSEWHEEL:
		ScreenCaptureFramework->MouseInput(hWnd, message, wParam, lParam);
		//if( (SHORT)HIWORD(wParam) > 0 )
		//	cout << HIWORD(wParam) << endl;
		////	cout << "마우스 휠 위로" << endl;
		//else
		//	cout << HIWORD(wParam) << endl;
		////	cout << "마우스 휠 아래로" << endl;
		break;
	case WM_CHAR: case WM_KEYDOWN: case WM_KEYUP:
		ScreenCaptureFramework->KeyInput(hWnd, message, wParam, lParam);
		break;
	case WM_DESTROY:
	case WM_CLOSE:
		PostQuitMessage(0);

		//Unregister for Application Restart
		UnregisterHotKey(hWnd, HK_SCREENSHOT);
		UnregisterHotKey(hWnd, HK_OPEN_LAST_SCREENSHOT);
		UnregisterHotKey(hWnd, HK_OPEN_DIRECTORY);
		UnregisterHotKey(hWnd, HK_LEFT_DIRECTORY);
		UnregisterHotKey(hWnd, HK_RIGHT_DIRECTORY);
		UnregisterHotKey(hWnd, HK_MONITOR_SWITCH_EXTEND);
		UnregisterHotKey(hWnd, HK_MONITOR_SWITCH_ONLY_MAIN);
		UnregisterHotKey(hWnd, HK_MONITOR_SWITCH_ONLY_SUB);
		UnregisterHotKey(hWnd, HK_MONITOR_SWITCH_DUPLICATE);

		g_bLoop = false;
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

INT_PTR About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
