#pragma once

#define ScreenCaptureFramework CScreenCaptureFramework::GetInstance()

class CScreenCaptureFramework
{
public:	
	static void CreateInstance(HWND hWnd);
	static CScreenCaptureFramework * GetInstance();
	void Initialize(HWND hWnd);
	void Release();

	void Update(float deltaTime);
	void Render();

	//Capture
	void FullCapture();
	void SelectCapture();
	void RegionCapture();

	void DrawOutLine();
	void LoadToClipBoard();
	void SaveCaputre();

	void MouseInput(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	void KeyInput(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	bool HotKey(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	void TaskBar(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam);
	void MenuSelect(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

private:
	CScreenCaptureFramework() {}
	~CScreenCaptureFramework() {}

private:
	static CScreenCaptureFramework* m_pInstance;

	HWND	m_hWnd;

	bool	m_bFirstExecution = false;
	bool	m_bLeftMouseButton = false;

	//Mouse Points
	POINT	m_preMousePos;
	POINT	m_nowMousePos;

	HDC		m_hCapturedDC;
	HBITMAP m_hCapturedBitmap;
	HDC		m_hSelectedDC;
	HBITMAP m_hSelectedBitmap;
	std::wstring _lastScreenshotFileName;
};

_declspec(selectany) CScreenCaptureFramework* CScreenCaptureFramework::m_pInstance = nullptr;

