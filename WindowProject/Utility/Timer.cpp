#include "stdafx.h"
#include "Timer.h"

void CTimer::Initialize(int nLockFPS) 
{
	__int64 countsPereSec;
	QueryPerformanceFrequency((LARGE_INTEGER*)&countsPereSec);
	m_dSecondsPerCount = 1.0 / (double)countsPereSec;
	QueryPerformanceCounter((LARGE_INTEGER*)&m_nPrevTime);
	m_nCurrTime = 0;
	m_dLockTime = 1.0f / nLockFPS;
}

float CTimer::Update() 
{
	//Real Elapsed Time 계산
	QueryPerformanceCounter((LARGE_INTEGER*)&m_nCurrTime);
	auto advanceTime = (m_nCurrTime - m_nPrevTime) * m_dSecondsPerCount;

	//타이머 Update가 중단되었다가 재가동 되었을때 이슈 해결.
	if (advanceTime > 0.03)
		advanceTime = 0;

	m_dDeltaTime = advanceTime;

	//FPS Time Lock
	__int64	nPrevTime;
	__int64	nCurrTime;

	QueryPerformanceCounter((LARGE_INTEGER*)&nPrevTime);
	while (true) 
	{
		QueryPerformanceCounter((LARGE_INTEGER*)&nCurrTime);
		m_dDeltaTime += (nCurrTime - nPrevTime) * m_dSecondsPerCount;
		if (m_dDeltaTime >= m_dLockTime) 
		{
			m_dWaitTime = m_dDeltaTime - advanceTime;
			
			if (m_dDeltaTime < 0.0) {
				m_dDeltaTime = 0.0;
			}
			break;
		}
		nPrevTime = nCurrTime;
	}

	QueryPerformanceCounter((LARGE_INTEGER*)&m_nPrevTime);
	return (float)m_dDeltaTime;
}