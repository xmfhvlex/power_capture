#pragma once

#define TimerEngine CTimer::GetInstance()

class CTimer {
private:
	static CTimer* m_pInstance;

	// Time Lock이 유지된 시간
	double		m_dWaitTime			= 0;
	// Wait Time + Advance Time (Scene Update에 사용되는 최종 Elapsed Time)
	double		m_dDeltaTime		= -1;

	double		m_dSecondsPerCount	= 0;

	// Time Lock 시간
	double		m_dLockTime			= 00;

	__int64		m_nPrevTime			= 0;
	__int64		m_nCurrTime			= 0;

private:
	CTimer() {}
	~CTimer() {}

public:
	static CTimer* GetInstance() 
	{ 
		if (m_pInstance == nullptr) {
			m_pInstance = new CTimer;
			m_pInstance->Initialize();
		}
		return m_pInstance; 
	}

	void Initialize(int nLockFPS = 60);

	float Update();

	float GetElapsedTime() {
		return (float)m_dDeltaTime;
	}
};

__declspec(selectany) CTimer* CTimer::m_pInstance = nullptr;