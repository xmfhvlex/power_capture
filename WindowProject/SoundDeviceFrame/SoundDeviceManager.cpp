#include "stdafx.h"
#include "SoundDeviceManager.h"
#include "WindowFramework.h"
#include "D2DFramework.h"
#include <iterator>

#define _CRT_SECURE_NO_WARNINGS 1

void CSoundDeviceManager::Initialize() 
{
	PrepareDeviceList();

	//HotKey Register
	RegisterHotKey(g_hwnd, HK_LEFT_SOUND_DEVICE, MOD_CONTROL | MOD_SHIFT | MOD_ALT, 0X26);
	RegisterHotKey(g_hwnd, HK_RIGHT_SOUND_DEVICE, MOD_CONTROL | MOD_SHIFT | MOD_ALT, 0x28);
}

void CSoundDeviceManager::PrepareDeviceList()
{
	IMMDeviceEnumerator *pEnum = nullptr;      // Audio device enumerator.
	IMMDeviceCollection *pDevices = nullptr;   // Audio device collection.
	IMMDevice* pDevice = nullptr;
	IPropertyStore *pProps = nullptr;

	CoInitialize(nullptr);

	if (FAILED(CoCreateInstance(__uuidof(MMDeviceEnumerator), nullptr, CLSCTX_ALL, __uuidof(IMMDeviceEnumerator), (void**)&pEnum)))
		return;

	if (FAILED(pEnum->EnumAudioEndpoints(eRender, DEVICE_STATE_ACTIVE, &pDevices)))
		return;

	if (FAILED(pEnum->GetDefaultAudioEndpoint(EDataFlow(), ERole(), &pDevice)))
		return;

	WCHAR* pCurrDeviceName;
	pDevice->GetId(&pCurrDeviceName);

	m_vwDeviceID.clear();
	m_vwDeviceName.clear();

	cout << "=================================" << endl;
	cout << "PrepareDeviceList" << endl;
	pDevices->GetCount(&m_nCount);
	cout << "Num of Device : " << m_nCount << endl;


	int ignoreIdx = -1;

	m_vwDeviceID.resize(m_nCount);
	for (int i = 0; i < m_nCount; ++i) 
	{
		pDevices->Item (i, &pDevice);
		pDevice->GetId(&m_vwDeviceID[i]);
		pDevice->OpenPropertyStore(STGM_READ, &pProps);
        PROPVARIANT varName;
        PropVariantInit(&varName);
        pProps->GetValue(PKEY_Device_FriendlyName, &varName);
		m_vwDeviceName.push_back(varName.pwszVal);
		std::cout << "[" << i << "] : " << varName.pwszVal << endl;

		//Set Current SoundDevice ID
		if (wcscmp(pCurrDeviceName, m_vwDeviceID[i]) == 0) 
		{
			m_pvtID = i;
			std::cout << "Current DeviceID : " << i << endl;
		}

		if (lstrcmpW(m_vwDeviceID[i], L"{0.0.0.00000000}.{099bccfd-d24e-40cc-aa91-441b01db35bc}") == 0)
			ignoreIdx = i;

		pProps->Release();
	}

	if (ignoreIdx != -1) 
	{
		m_vwDeviceID.erase(m_vwDeviceID.begin() + ignoreIdx);
		m_vwDeviceName.erase(m_vwDeviceName.begin() + ignoreIdx);
	}

	pEnum->Release();
	pDevices->Release();
}

std::wstring CSoundDeviceManager::GetNowDeviceName() {
	return m_vwDeviceName[m_pvtID];
}

HRESULT CSoundDeviceManager::SetDefaultAudioPlaybackDevice(LPCWSTR devID)
{
	IPolicyConfig *pPolicyConfig;
	ERole reserved = eConsole;

	HRESULT hr = CoCreateInstance(
		__uuidof(CPolicyConfigClient),
		NULL,
		CLSCTX_ALL,
		__uuidof(IPolicyConfig),
		(LPVOID *)&pPolicyConfig);

	if (SUCCEEDED(hr))
	{
		hr = pPolicyConfig->SetDefaultEndpoint(devID, reserved);
		pPolicyConfig->Release();
	}

	return hr;
}

bool CSoundDeviceManager::HotKey(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (wParam) {
	case HK_LEFT_SOUND_DEVICE:
		m_pvtID--;

		if (m_pvtID < 0 || m_pvtID == m_vwDeviceID.size())
		{
			m_pvtID = m_vwDeviceID.size() - 1;
		}

		std::cout << m_vwDeviceID[m_pvtID] << std::endl;

		SetDefaultAudioPlaybackDevice(m_vwDeviceID[m_pvtID]);
		D2DFramework->Message(EMessage::SoundDeviceChanged);
		return true;

	case HK_RIGHT_SOUND_DEVICE:
		m_pvtID++;
		if (m_pvtID < 0 || m_pvtID == m_vwDeviceID.size())
		{
			m_pvtID = 0;
		}

		std::cout << m_vwDeviceID[m_pvtID] << std::endl;

		SetDefaultAudioPlaybackDevice(m_vwDeviceID[m_pvtID]);
		D2DFramework->Message(EMessage::SoundDeviceChanged);
		return true;
	}
	return false;
}
