#pragma once

#include <mmdeviceapi.h>
#include "PolicyConfig.h"


#define SoundDeviceManager		CSoundDeviceManager::Instance()
class CSoundDeviceManager {
public:
	CSoundDeviceManager() {}
	~CSoundDeviceManager() {}

	static CSoundDeviceManager* Instance() 
	{
		static CSoundDeviceManager* pInstance = nullptr;

		if (pInstance == nullptr) {
			pInstance = new CSoundDeviceManager;
			pInstance->Initialize();
		}

		return pInstance;
	}

	void Initialize();

	//Sound Output Device를 List화 합니다.
	void PrepareDeviceList();

	std::wstring GetNowDeviceName();

	HRESULT SetDefaultAudioPlaybackDevice(LPCWSTR devID);

	bool HotKey(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

private:
	std::vector<WCHAR*>			m_vwDeviceID;
	std::vector<WCHAR*>			m_vwDeviceName;
	int							m_pvtID=0;
	UINT						m_nCount = 0;
};
